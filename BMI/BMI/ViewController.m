//
//  ViewController.m
//  BMI
//
//  Created by hcc on 2014/2/12.
//  Copyright (c) 2014年 hcc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *bodyHeight;
@property (weak, nonatomic) IBOutlet UITextField *bodyWeight;
@property (weak, nonatomic) IBOutlet UILabel *resultBMI;


@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonClick:(id)sender {
    CGFloat weight = [self.bodyWeight.text floatValue];
    CGFloat height = [self.bodyHeight.text floatValue] / 100;
    CGFloat result = weight / (height * height);
    self.resultBMI.text = [NSString stringWithFormat:@"%f", result];
}

- (IBAction)hhh:(id)sender {
}
@end
